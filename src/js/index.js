jQuery(document).ready(function () {

  $(".banner-slider-big").slick({
    infinity: true,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrow: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          fade: false,
          dots: false,
          variableWidth: true,
          swipeToSlide: true
        }
      }
    ]
  });

  $(".slider-popular").slick({
    infinity: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    arrow: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          dots: false,
          variableWidth: true,
          swipeToSlide: true
        }
      }
    ]
  });


  $(".slider-contacts").slick({
    infinity: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: false,
    arrow: false,
    swipeToSlide: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          variableWidth: true,
          slidesToShow: 1
        }
      }
    ],

  });

  $('.slider-about').slick({
    infinity: true,
    slidesToScroll: 1,
    slidesToShow: 3,
    arrows: true,
    dots: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          dots: false,
          variableWidth: true,
          swipeToSlide: true
        }
      }
    ]
  });

  $('.slider-team').slick({
    infinity: true,
    slidesToScroll: 1,
    slidesToShow: 3,
    arrows: true,
    dots: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          dots: false,
          variableWidth: true,
          swipeToSlide: true
        }
      }
    ]
  });


  $('.modal-open').modal('show');

  $('.input, .select, .input-radio, .input-checkbox').styler();

  $(function () {
    $('.phone').mask('+7 (999) 999-99-99');
  });

  $(function () {
    $('.promo').mask('999 - 99 - 999');
  });

  $('.item-active-js').on('click', function () {
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
  });



  $('.accord-title--js').on('click', function () {
    var parents = $(this).closest('.accord-wrap--js');
    var h_block = $(this).siblings('.filter-accord__block').height();
    $(this).find('.checkbox__block-nano').toggleClass('show-height');
    parents.children('.accord-block--js').slideToggle();
    parents.toggleClass('active');
    console.log(h_block);
    if (h_block > 399) {
      var parents = parents;
      parents.find('.checkbox__block-nano').addClass('nano');
      parents.find('.checkbox__block-nano-content').addClass('nano-content');
      $('.nano').nanoScroller();
    }
    ;
  });


// Card's slider
  $.each($('.product-item__slider'), function () {
  var $carousel = $(this);

  $carousel
    .slick({
      infinity: true,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrow: false,
      swipeToSlide: true,
      asNavFor: $(this).parents('.slider__wrap').find('.product-item__slider-nav'),

    })
    .magnificPopup({
      type: 'image',
      delegate: 'a:not(.slick-cloned)',
      closeOnContentClick: false,
      tLoading: 'Загрузка...',
      mainClass: 'mfp-zoom-in mfp-img-mobile',
      image: {
        verticalFit: true,
        tError: '<a href="%url%">Фото #%curr%</a> не загрузилось.'
      },
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        tCounter: '<span class="mfp-counter">%curr% из %total%</span>', // markup of counte
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
      },
      zoom: {
        enabled: true,
        duration: 300
      },
      removalDelay: 300, //delay removal by X to allow out-animation
      callbacks: {
        buildControls: function() {
          // re-appends controls inside the main container
          this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
        },
        open: function () {
          //overwrite default prev + next function. Add timeout for css3 crossfade animation
          $.magnificPopup.instance.next = function () {
            var self = this;
            self.wrap.removeClass('mfp-image-loaded');
            setTimeout(function () {
              $.magnificPopup.proto.next.call(self);
            }, 120);
          };
          $.magnificPopup.instance.prev = function () {
            var self = this;
            self.wrap.removeClass('mfp-image-loaded');
            setTimeout(function () {
              $.magnificPopup.proto.prev.call(self);
            }, 120);
          };
          var current = $carousel.slick('slickCurrentSlide');
          $carousel.magnificPopup('goTo', current);
        },
        imageLoadComplete: function () {
          var self = this;
          setTimeout(function () {
            self.wrap.addClass('mfp-image-loaded');
          }, 16);
        },
        beforeClose: function () {
          $carousel.slick('slickGoTo', parseInt(this.index));
        }
      }
    });
  });
  $.each($('.product-item__slider-nav'), function () {
    $(this).slick({
      infinity: true,
      slidesToScroll: 1,
      slidesToShow: 3,
      asNavFor: $(this).parents('.slider__wrap').find('.product-item__slider'),
      focusOnSelect: true,
      arrows: true,
      prevArrow: '<a href="#" class="prev-button"></a>',
      nextArrow: '<a href="#" class="next-button"></a>',
      appendArrows: $(this).parents('.slider__wrap').find('.container-arrows'),
    });
  });

  
  


  $('.icon-tiles').on('click', function () {
    $('.catalog__block.tiles').addClass('active');
    $('.catalog__block.list').removeClass('active');
    $('.readmore').readmore({
      speed: 700,
      maxHeight: 76,
      moreLink: '<div class="show-more"><span>читать полностью ...</span></div>',
      lessLink: '<div class="show-more"><span>свернуть</span></div>'
    });
  });


  $('.icon-list').on('click', function () {
    $('.catalog__block.tiles').removeClass('active');
    $('.catalog__block.list').addClass('active');
    $('.product-item__slider').slick('setPosition');
    $('.product-item__slider-nav').slick('setPosition');

  });

  $('.button-favor').on('click', function () {
    $(this).toggleClass('add');
  });




  $('.select-city').select2({
    placeholder: "Выберите город",
    allowClear: true,
  });




  $('.tabs-item--js').on('click', function () {
    var itemThumbs = $(this).attr('data-thumb'),
      imgThumbs = $('.tabs__block--js[data-thumb= \'' + itemThumbs + '\']');
    $(this).addClass('active').siblings().removeClass('active');
    imgThumbs.addClass('active').siblings().removeClass('active');
  });

  $('.button-address__wrap').on('click', function () {
    $('.address-list').toggleClass('active');
  });

  $('.datepicker-input').datepicker({
    autoClose: true,
    minDate: new Date()
  });


  $('.edit-button').click(function (event) {
    $(this).parents('.edit-buttons').addClass('save');
    event.preventDefault();
    $(this).parents('.lk-address__item').find('.input-text').prop('disabled', false);
  });

  $('.save-button').click(function (event) {
    $(this).parents('.edit-buttons').removeClass('save');
    event.preventDefault();
    $(this).parents('.lk-address__item').find('.input-text').prop('disabled', true);
  });


  var mql = window.matchMedia('(max-width: 991px)');

  function screenTest(e) {
    if (e.matches) {
      $(".slider-brands").slick({
        infinity: true,
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        arrow: false,
        swipeToSlide: true
      });
      $(".slider-feedback").slick({
        infinity: true,
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        arrow: false,
        swipeToSlide: true
      });
      $(".slider-products").slick({
        infinity: true,
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        arrow: false,
        swipeToSlide: true
      });
      $(".slider-news-main").slick({
        infinity: true,
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        arrow: false,
        swipeToSlide: true
      });
      $(".slider-banner-main").slick({
        infinity: false,
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        arrow: false,
        swipeToSlide: true
      });
      $(".custom-scrollbar").mCustomScrollbar({
        axis: 'x'
      });

    }
  }

  mql.addListener(screenTest);
  screenTest(mql);

  $(window).on("scroll", function () {
    var scrolled = $(this).scrollTop();
    if (scrolled > 126) {
      $(".header-float").addClass("active");
    }
    if (scrolled <= 126) {
      $(".header-float").removeClass("active");
    }
  });

  $('.readmore').readmore({
    speed: 700,
    maxHeight: 76,
    moreLink: '<div class="show-more"><span>читать полностью ...</span></div>',
    lessLink: '<div class="show-more"><span>свернуть</span></div>'
  });


  $('.humb-block').on('click', function () {
    $('.body').addClass('menu-mob-open');
  });
  $('.humb-red-block').on('click', function () {
    $('.body').addClass('catalog-mob-open');
  });

  $('.button-close').on('click', function () {
    $('.body').removeClass('menu-mob-open');
    $('.body').removeClass('catalog-mob-open');
  });
  $(document).on('click', function (e) {
    if (!$(e.target).closest(".humb-block, .humb-red-block, .menu-mob__wrap, .catalog-mob__wrap").length) {
      $('.body').removeClass('menu-mob-open');
      $('.body').removeClass('catalog-mob-open');

    }
    e.stopPropagation();
  });

  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {

      if ($('#upbutton').is(':hidden')) {
        $('#upbutton').css({opacity: 1}).fadeIn('slow');

      }
    } else {
      $('#upbutton').stop(true, false).fadeOut('fast');
    }
    $('.img-arrow-down').hide();
  });
  $('#upbutton').click(function () {
    $('html, body').stop().animate({scrollTop: 0}, 300);
  });
  
  



});





