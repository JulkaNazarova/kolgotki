
if ($("#map").length > 0) {
  ymaps.ready(init);
  function init() {
    var contactsMap = new ymaps.Map("map", {
      center: [55.665198, 37.641027],
      zoom: 16,
      controls: []
    });
    closeBalloon = function () {
      contactsMap.balloon.close();
    };
    var contactsPlacemark = new ymaps.Placemark(
      [55.665198, 37.641027],
      {},
      {
        iconLayout: "default#imageWithContent",
        iconImageHref: "../img/myicon.svg",
        iconImageSize: [36, 50],
        iconImageOffset: [-10, -60],
        // balloonLayout: MyBalloonLayout,
        hideIconOnBalloonOpen: false
      }
    );
    contactsMap.geoObjects.add(contactsPlacemark);
    contactsMap.behaviors.disable("scrollZoom");
    contactsMap.behaviors.disable('drag');
  }
}



